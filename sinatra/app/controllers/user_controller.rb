class UsersController < ApplicationController
  get '/signup' do
    if logged_in?
      redirect to "/users/#{current_user.username}"
    else
      erb :"/users/signup"
    end
  end

  post '/signup' do
    if params[:username].empty? || params[:email].empty? || params[:password].empty?
      redirect to '/signup'
    else
      @user = User.create(username: params[:username], email: params[:email], password: params[:password], password_confirmation: params[:password_confirmation])
      session[:username] = @user.username
      redirect to "/"
    end
  end
end
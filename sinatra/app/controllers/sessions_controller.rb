class SessionsController < ApplicationController
  get '/login' do
    if logged_in?
      redirect to '/'
    else
      erb :login
    end
  end

  post '/auth/login' do
    @user = User.find_by(username: params[:username])
    puts User.find_by(username: params[:username])
    if @user && @user.authenticate(params[:password])
      session[:username] = @user.username
      redirect to '/'
    elsif params[:username].empty? || params[:password].empty?
      flash.now[:notice] = "Username or password field was empty"
      erb :login
    else
      erb :login
    end
  end

  delete '/logout' do
    session.destroy
    redirect to '/'
  end

end

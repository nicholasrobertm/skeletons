class ApplicationController < Sinatra::Base

  configure do
    set :sessions, true
    set :session_secret, ENV['SESSION_SECRET']
    set :database, adapter: "sqlite3", database: "development.sqlite3"
    set :public_folder, 'public'
    set :views, 'app/views'
    register Sinatra::Flash
  end

  # This is the Hello World route (so that you can see that the code is running).
  get '/' do
    erb :index, :layout => :layout
  end

  get '/protected' do
    if logged_in?
      'This path is protected'
    else
      redirect_if_not_logged_in
    end
  end

  helpers do

    def logged_in?
      !!session[:username]
    end

    def current_user
      User.find_by(:username => session[:username])
    end

    def redirect_if_not_logged_in
      redirect to '/login' unless logged_in?
    end
  end

end